var babelify = require('babelify');
var browserify = require('browserify');
var buffer = require('vinyl-buffer');
var debug = require('gulp-debug');
var gulp = require('gulp');
var Server = require('karma').Server;
var less = require('gulp-less');
var rjs = require('gulp-requirejs');
var source = require('vinyl-source-stream');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('rjs', function () {
    return rjs({
        name: 'requirejs.main',
        baseUrl: 'src/',
        out: 'requirejs.main.js',
        paths: {
            jquery: '../node_modules/jquery/dist/jquery'
        }
    }).pipe(gulp.dest('./dist/js'));
});

gulp.task('browserify', function () {
  var b = browserify('src/browserify.index.js', {debug: true});

  return b
    .transform(babelify)
    .bundle()
    .pipe(source('browserify.bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/js'))
});

gulp.task('html', function () {
    return gulp.src('src/**.html')
        .pipe(gulp.dest('./dist/'));
});

gulp.task('css', function () {
    return gulp.src('node_modules/@atlassian/aui-adg/dist/aui/css/**/*')
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('build', ['rjs', 'browserify', 'html', 'css']);

gulp.task('default', function () {
    gulp.watch('src/**/*', ['build']);
});

gulp.task('test', function(done) {
    browserify('tests/src/browserify.js', {debug: true})
        .transform(babelify)
        .bundle()
        .pipe(source('browserify.js'))
        .pipe(buffer())
        .pipe(gulp.dest('./tests/dist'))
        .on('error', function(err) {
            // Make sure failed tests cause gulp to exit non-zero
            throw err;
        })
        .on('end', function() {
            server = new Server({
                configFile: __dirname + '/karma.conf.js'
            }, done);
            server.start();
        });
});
