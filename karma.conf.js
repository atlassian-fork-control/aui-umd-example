module.exports = function(config) {
    config.set({
        reporters: ['progress', 'junit'],
        files: [
            'tests/dist/*.js'
        ],
        frameworks: ['mocha', 'sinon-chai', 'jquery-1.8.3'],
        colors: true,
        logLevel: 'info',
        singleRun: true,
        browsers: ['Chrome'],
        reportSlowerThan: 500
    })
};
